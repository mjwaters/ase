.. A new scriv changelog fragment.
..
.. Uncomment the header that is right (remove the leading dots).
..
I/O
---

- Added :func:`~ase.io.castep.write_castep_geom` and
  :func:`~ase.io.castep.write_castep_md` (:mr:`3229`)

.. Calculators
.. -----------
..
.. - A bullet item for the Calculators category.
..
.. Optimizers
.. ----------
..
.. - A bullet item for the Optimizers category.
..
.. Molecular dynamics
.. ------------------
..
.. - A bullet item for the Molecular dynamics category.
..
.. GUI
.. ---
..
.. - A bullet item for the GUI category.
..
.. Development
.. -----------
..
.. - A bullet item for the Development category.
..
.. Other changes
.. -------------
..
.. - A bullet item for the Other changes category.
..
.. Bugfixes
.. --------
..
.. - A bullet item for the Bugfixes category.
..
